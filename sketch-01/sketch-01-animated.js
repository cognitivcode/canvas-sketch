const canvasSketch = require('canvas-sketch');

const settings = {
  dimensions: [ 1080, 1080 ],
  animate: true
};

const animate = () => {
  requestAnimationFrame(animate)
}

const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'black';
    context.fillRect(0, 0, width, height);
    context.strokeStyle = 'white'
    context.lineWidth = width * 0.002
    
    const w   = width * 0.10
    const h   = height * 0.10
    const gap = width * 0.03
    const marginx  = width * 0.17
    const marginy  = height * 0.17

    const off = width * 0.02

    let x, y
    
    for (let i = 0; i < 5; i++) {
      
      for (let j = 0; j < 5; j++) {
        
        x = marginx + (w + gap) * i
        y = marginy + (h + gap) * j
        
        context.beginPath()
        context.rect(x, y, w, h)
        context.stroke()
        
        if (Math.random() > 0.5) {
          offRand = off * Math.random() * 5
          context.beginPath()
          context.rect(x + offRand / 2, y + offRand / 2, w - offRand, h - offRand)
          context.stroke()
        }          
      }

    }
  };
};

canvasSketch(sketch, settings);
