<div style="text-align: center; font-size: 25px;">
    #!?=) Creative Coding | Bitácora de Curso !="?¡!
</div>

----------------------------------------

La razón de este documento es crear una  bitacora para ir documentando lo que voy aprendiendo en el curso, a modo de guia, ayuda memoria y demás

----------------------------------------

#### Unidad 1 | Introducción 

----------------------------------------

El vato nos introduce al concepto de código creativo, nos cuenta un poco su historia y nos comparte sus influencias, las cuales algunas son increiblemente interesantes por lo que voy a dejar algunos links acá:

###### Yugo Nakamura
+  [JamPack](https://www.youtube.com/watch?v=dNT-NcCIPtA)
+ [Borde](https://www.youtube.com/watch?v=fu3jtG0SwU8)
+ [KASHIWASATO.COM para iPad](https://vimeo.com/43805654)
+ [THA LTD.](https://tha.jp)

###### Justin Windle
+ [Sketch.js](https://soulwire.github.io/sketch.js/)

###### Eiji Muroichi
+ [Playpit](http://playpit.kowareru.com/)

###### Memo Akten
+ [Baile de ameba](http://www.memo.tv/works/amoeba-dance/)
+ [Movimiento armónico simple n. ° 5](http://www.memo.tv/works/simple-harmonic-motion-5/)
+ [Formularios](http://www.memo.tv/works/forms/)

###### Ryoji Ikeda
+ [X-verse](https://www.ryojiikeda.com/project/x_verse/)
+ [Supersymmetry](https://www.ryojiikeda.com/project/supersymmetry/)

###### Matt DesLauriers
+ [Perfil de Instagram](https://www.instagram.com/mattdesl_art/)

###### Anders Hoff
+ [Arte digital](https://img.inconvergent.net/generative/)

----------------------------------------

#### Unidad 2 | Fundamentos

----------------------------------------

En esta sección vemos un poco las bases de que vamos a ver en el curso, instalamos la libreria `canvas-sketch` y empezamos a codear. El sketch-01 es el reflejo de lo que nos explica en esta sección, vimos:

+ Como estilizar el lienzo (cambiar color de fondo, tamaño, etc)
+ Dibujar formas con diferentes tamaños de trazos y en diferentes lugares
+ Generar un dibujo de una cuadricula con for-loops
+ Guardar/descargar diseños

<div style="display: grid; grid-template-columns: 50% 50%; padding: 20px; width: 100%;">
    <img src="./sketch-01/output/sketch-01-white.png"></img>
    <img src="./sketch-01/output/sketch-01-black.png"></img>
</div>

----------------------------------------

#### Unidad 3 | Sketch Transform

----------------------------------------

Acá el chango muestra como funciona la cuestión de la posición, como podemos transformar más aun nuestras formas basicas, rotarlas, estirarlas, achicarlas, escalarlas, etc. Primero comienza  con un cuadrado, luego lo achica para convertirlo en una especie de linea que despues gracias a la bendita trigonometria utiliza para crear una suerte de reloj, utilizando nuevamente un for-loop.

<div style="display: flex; padding: 20px; width: 100%; justify-content: center;">
    <img src="./sketch-02/output/sketch-02-reloj-1.png" style="width: 300px; border-radius: 20px;"></img>
</div>

Toqueteando un poco se pueden hacer cosas locas, por ejemplo en las siguientes imagenes fui jugando con la cantidad de elementos (slices), la escala, el ángulo, etc.

<div style="display: grid; grid-template-columns: 50% 50%; padding: 20px; width: 100%; grid-gap: 20px">
    <img src="./sketch-02/output/sketch-02-flash-1.png" style="border-radius: 20px;"></img>
    <img src="./sketch-02/output/sketch-02-flash-2.png" style="border-radius: 20px;"></img>
</div>

Luego muestra que hay otra libreria muy util, llamada `canvas-sketch-util` la cual tiene banda de funciones auxiliares y utiles para ahorrar tiempo y hacer otras  cosas zarpadas. Reemplazamos algunas funciones que habiamos creado anteriormente por las de esta librería.

El reloj se va transformando en un espiral digital extraño, acá el proceso:

<div style="display: grid; grid-template-columns: 25% 25% 25% 25%; padding: 20px; width: 100%; align-self: center;">
    <img src="./sketch-02/output/sketch-02-reloj-1.png" style="width: 300px; border-radius: 20px 0 0 0;"></img>
    <img src="./sketch-02/output/sketch-02-reloj-2.png" style="width: 300px;"></img>
    <img src="./sketch-02/output/sketch-02-reloj-3.png" style="width: 300px;"></img>
    <img src="./sketch-02/output/sketch-02-reloj-4.png" style="width: 300px; border-radius: 0 0 20px 0;"></img>
</div>

Al final de la unidad el resultado se presta a la experimentación, por lo que me puse a jugar con la cantidad de slices y los colores del sketch,uno de los outputs que más me gusto fue el siguiente:

<div style="display: flex; padding: 20px; width: 100%; justify-content: center;">
    <img src="./sketch-02/output/sketch-02-final.png" style="border-radius: 20px;"></img>
</div>

Antes de avanzar a la siguiente unidad experimenté con unos espirales logrando un resultado muy interesante:

<div style="display: flex; padding: 20px; width: 100%; justify-content: center;">
    <img src="./sketch-02/output/sketch-02-spiral.png" style="border-radius: 20px;"></img>
</div>

----------------------------------------

#### Unidad 4 | Sketch Agents

----------------------------------------

En esta unidad utilizamos clases de Javascript para crear objetos con comportamientos similares, definiendoles propiedades y funciones. Luego nos sumergimos en las animaciones viendo conceptos como el teorema de Pitágoras para calcular la cercania entre los diferentes objetos de nuestro sketch.

Nos explica tambien que para poder guardar los videos generados tenemos que ponerle al comando de `canvas-sketch` el parámetro `--stream`. Luego en vez de `ctrl + s` (que es para guardar un snapshot del diseño) tocamos `ctrl + shift + s` para iniciar la grabación de un video y volvemos a tocar lo mismo para terminar el video, de esa forma se nos guarda en donde hallamos definido.

> TIP: para decirle a `canvas-sketch` donde queremos que nos guarde las capturas y videos podemos definirle un `--output=la-carpeta-o-ruta-que-queramos`.

Para convertir los videos a gifs podemos usar el siguiente comando ([cortesía de un vato en stackoverflow](https://askubuntu.com/questions/648603/how-to-create-an-animated-gif-from-mp4-video-via-command-line)):

```
ffmpeg \
  -i nombre.mp4 \
  -r 15 \
  -vf scale=512:-1 \
  -ss 00:00:03 -to 00:00:06 \
  nombre.gif
```

`-r` son los fps, `-vf` es el tamaño, `-ss` el tiempo de start y end

El resultado inicial es el siguiente:

<div style="display: flex; padding: 20px; width: 100%; justify-content: center;">
    <img src="./sketch-03/output/sketch-03-init.gif" style="border-radius: 20px;"></img>
</div>

Luego limpiamos un poco el diseño e hicimos que las lineas sean tengan un grosor variable dependiendo de la cercanía de los puntos:

<div style="display: flex; padding: 20px; width: 100%; justify-content: center;">
    <img src="./sketch-03/output/sketch-03-final.gif" style="border-radius: 20px;"></img>
</div>

Y después de experimentar un buen rato el resultado fue el siguiente:

<div style="display: flex; padding: 20px; width: 100%; justify-content: center;">
    <img src="./sketch-03/output/sketch-03-experiment.gif" style="border-radius: 20px;"></img>
</div>