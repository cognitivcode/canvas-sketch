const canvasSketch = require('canvas-sketch');
const { random } = require('canvas-sketch-util');
const { math } = require('canvas-sketch-util');

const settings = {
  dimensions: [ 1080, 1080 ],
  animate: true
};

const animate = () => {
  console.log('The following function is which canvas-sketch uses in background when we set the animate: true property')
  requestAnimationFrame(animate)
}

// And here we call the function once, and because it is a recursive function it will be called at x FPS
// animate()

const sketch = ({ context, width, height }) => {
  const agents = []
  const stars = []

  const container = { x: 150, y: 150 }
  for (let i = 0; i < 10; i++) {
    const x = random.range(0 + container.x, width - container.x)
    const y = random.range(0 + container.y, height - container.y)
    
    agents.push(new Agent(x, y))
  }

  for (let i = 0; i < 300; i++) {
    const x = random.range(0, width)
    const y = random.range(0, height)
    
    stars.push(new Star(x, y))
  }

  return ({ context, width, height }) => {
    context.fillStyle = `rgb(${random.range(10, 12)},${random.range(10, 13)},${random.range(10, 22)})`
    context.fillRect(0, 0, width, height);

    for (let i = 0; i < agents.length; i++) {

      context.fillStyle = 'white'
      context.strokeStyle = `rgb(${random.range(0, 256)},${random.range(0, 256)},${random.range(0, 256)})`

      const agent = agents[i];
      
      for (let j = i + 1; j < agents.length; j++) {
        const other = agents[j];
        
        const dist = agent.pos.getDistance(other.pos)

        // Comment this two lines out for funny results
        // if (dist > 200) continue
        // context.lineWidth = math.mapRange(dist, 0, 200, 3, 1)

        context.beginPath()
        context.moveTo(agent.pos.x, agent.pos.y)
        context.lineTo(other.pos.x, other.pos.y)
        context.stroke()
      }
    }

    agents.forEach(agent => {
      agent.update()
      agent.draw(context)
      
      // Choose btw this two alternatives of movement
      // agent.bounce(width, height)
      agent.bounceContained(width, height, container.x, container.y)
      // agent.wrap(width, height)
    })

    // for (let i = 0; i < stars.length; i++) {
    //   const star = stars[i];
    // }

    stars.forEach(star => {
      star.updateWithOrbitMovement(width, height)
      star.draw(context)
      star.wrap(width, height)
    })
  };
};

canvasSketch(sketch, settings);

class Vector {
  constructor(x, y) {
    this.x = x
    this.y = y
  }

  getDistance(v) {
    const dx = this.x - v.x
    const dy = this.y - v.y
    return Math.sqrt(dx * dx + dy * dy)
  }
}

class Agent {
  constructor(x, y) {
    this.pos = new Vector(x, y)
    this.vel = new Vector(random.range(-1, 0.5), random.range(-1, 0.5))
    this.radius = random.range(4, 12)
  }

  draw(context) {
    context.save()
    context.translate(this.pos.x, this.pos.y)

    context.lineWidth = 4

    context.beginPath()
    context.arc(0, 0, this.radius, 0, Math.PI * 2)
    context.fill()
    context.stroke()

    context.restore()
  }

  update() {
    this.pos.x += this.vel.x
    this.pos.y += this.vel.y
  }

  wrap(width, height) {
    if (this.pos.x < 0) this.pos.x = width
    if (this.pos.x > width) this.pos.x = 0
    
    if (this.pos.y < 0) this.pos.y = height
    if (this.pos.y > height) this.pos.y = 0
  }

  bounce(width, height) {
    if (this.pos.x <= 0 || this.pos.x >= width) this.vel.x *= -1
    if (this.pos.y <= 0 || this.pos.y >= height) this.vel.y *= -1
  }

  bounceContained(width, height, containerX, containerY) {
    if (this.pos.x <= 0 + containerX || this.pos.x >= width - containerX) this.vel.x *= -1
    if (this.pos.y <= 0 + containerY || this.pos.y >= height - containerY) this.vel.y *= -1
  }
}

class Star {
  constructor(x, y) {
    this.pos = new Vector(x, y)
    // this.vel = new Vector(0.5, 0)
    // Uncomment for Smooth movement
    this.vel = new Vector(0.4, random.range(-0.1, 0.1))
    this.radius = random.range(0.01, 0.2)
  }

  draw(context) {
    context.save()
    context.translate(this.pos.x, this.pos.y)

    context.lineWidth = 1
    context.strokeStyle = 'white'

    context.beginPath()
    context.arc(0, 0, this.radius, 0, Math.PI * 2)
    context.fill()
    context.stroke()

    context.restore()
  }

  update(width, height) {
    this.pos.x += this.vel.x 
    this.pos.y += this.vel.y 
  }

  quadrant(width, number) {
    return width / 8 * number
  }

  updateWithOrbitMovement(width, height) {
    this.pos.x += this.vel.x 

    if (0 <= this.pos.x && this.pos.x < this.quadrant(width, 1)) {
      this.pos.y += 0
    } 
    
    if (this.quadrant(width, 1) < this.pos.x && this.pos.x < this.quadrant(width, 2)) {
      this.pos.y += 0.11
    }

    if (this.quadrant(width, 2) < this.pos.x && this.pos.x < this.quadrant(width, 3)) {
      this.pos.y += 0.22
    }

    if (this.quadrant(width, 3) < this.pos.x && this.pos.x < this.quadrant(width, 4)) {
      this.pos.y += 0.33
    }

    if (this.quadrant(width, 4) < this.pos.x && this.pos.x < this.quadrant(width, 5)) {
      this.pos.y += 0.44
    }

    if (this.quadrant(width, 5) < this.pos.x && this.pos.x < this.quadrant(width, 6)) {
      this.pos.y += 0.55
    }

    if (this.quadrant(width, 6) < this.pos.x && this.pos.x < this.quadrant(width, 7)) {
      this.pos.y += 0.66
    }

    if (this.quadrant(width, 7) < this.pos.x && this.pos.x < this.quadrant(width, 8)) {
      this.pos.y += 0.77
    }
  }

  wrap(width, height) {
    if (this.pos.x < 0) this.pos.x = width
    if (this.pos.x > width) this.pos.x = 0
    
    if (this.pos.y < 0) this.pos.y = height
    if (this.pos.y > height) this.pos.y = 0
  }
}
