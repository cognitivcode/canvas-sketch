const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math');
const random = require('canvas-sketch-util/random');

const settings = {
  dimensions: [ 1080, 1080 ]
};

const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = '#fff9e6';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'black'

    const cx   = width * 0.5
    const cy   = height * 0.5
    const w  = width * 0.01
    const h  = height * 0.1
    let x, y

    const num = 155
    const radius = width * 0.3

    for (let i = 0; i < num; i++) {
      const slice = math.degToRad(360 / num)
      const angle = slice * i
      
      x = cx + radius * Math.sin(angle)
      y = cy + radius * Math.cos(angle)

      context.save()
      context.translate(x, y)
      context.rotate(-angle)
      context.scale(1, 1)
  
      context.beginPath()
      context.rect(-w *  0.5, -h * 0.5, w, h)
      context.fill()
      context.restore()
    }

    context.translate(cx, cy)
    context.beginPath()
    context.lineWidth = 10
    const a = 3.76
    const b = 3.76
    for (i = 0; i < 720; i++) {
        angle = 0.1 * i;
        x = 0 + (a + b * angle) * Math.sin(1.5 * angle);
        y = 0 + (a + b * angle) * Math.cos(1.5 * angle);

        context.lineTo(x, y);
    }
    context.strokeStyle = "#000";
    context.stroke();
  };
};

canvasSketch(sketch, settings);
