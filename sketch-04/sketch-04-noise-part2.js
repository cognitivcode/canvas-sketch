const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');
const math = require('canvas-sketch-util/math');
const Tweakpane = require('tweakpane');
const settings = {
  dimensions: [ 1080, 1080 ],
  animate: true
};

const params = {
  cols: 10,
  rows: 10,
  scaleMin: 1,
  scaleMax: 30,
  freq: 0.001,
  amp: 0.2,
  animate: true,
  frame: 0,
  lineCap: 'butt'
}

const sketch = () => {
  return ({ context, width, height, frame }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    // Define a number of columns and rows for the grid
    const cols = params.cols
    const rows = params.rows
    const numCells = cols * rows

    // Now we set up a grid of 80% of the total width and height
    const gridw = width * 0.8
    const gridh = height * 0.8
    // We calculate the width and height for each cell of the grid
    const cellw = gridw / cols
    const cellh = gridh / rows
    // We have to keep in mind the margin x and y position 
    const margx = (width - gridw) * 0.5
    const margy = (height - gridh) * 0.5

    for (let i = 0; i < numCells; i++) {
      // The Remainder operator helps us know which col are we iterating
      const col = i % cols
      // And the floor operator of the division between the index and the number 
      // of cols would tell us the row we are iterating
      const row = Math.floor(i / cols)
      
      // Here we determine the x and y position of each cell of the iteration
      const x = col * cellw
      const y = row * cellh
      // And also here we declare the width and height of the line we want to draw
      const w = cellw * 0.8
      const h = cellh * 0.8

      const f = params.animate ? frame : params.frame
      // const n = random.noise2D(x + frame * 10, y, params.freq)
      const n = random.noise3D(x, y, f * 5, params.freq)

      const angle = n * Math.PI * params.amp
      // const scale = (n + 1) / 2 * 30
      // const scale = (n * 0.5 + 0.5) * 30
      const scale = math.mapRange(n, -1, 1, params.scaleMin, params.scaleMax)

      // We want to save the context and restore it on every iteration
      context.save()

      // We want to start to draw on the x and y position of each cell 
      context.translate(x, y)
      // But we have to add the margin x and y of the grid too
      context.translate(margx, margy)
      // And to move to the center of the cell and draw from there we do the following
      context.translate(cellw * 0.5, cellh * 0.5)

      context.rotate(angle)

      context.lineWidth = scale
      context.lineCap = params.lineCap

      // Now that we are at the center of the cell we start our path
      context.beginPath()
      // Move half to the left
      context.moveTo(w * -0.5, 0)
      // And then draw the line to half to the right
      context.lineTo(w * 0.5, 0)
      // Resulting on a line of 80% of the cell width (due to the margin)
      context.stroke()

      context.restore()
    }
  };
};

const createPane = () => {
  const pane = new Tweakpane.Pane()
  let folder

  folder = pane.addFolder({ title: 'Grid'})
  folder.addInput(params, 'lineCap', { options: { butt: 'butt', round: 'round', square: 'square'} })

  folder.addInput(params, 'cols', { min: 2, max: 50, step: 1 })
  folder.addInput(params, 'rows', { min: 2, max: 50, step: 1 })
  folder.addInput(params, 'scaleMin', { min: 1, max: 100 })
  folder.addInput(params, 'scaleMax', { min: 1, max: 100 })

  folder = pane.addFolder({ title: 'Noise'})
  folder.addInput(params, 'freq', { min: -0.1, max: 0.1 })
  folder.addInput(params, 'amp', { min: 0, max: 1 })
  
  folder.addInput(params, 'animate')
  folder.addInput(params, 'frame', { min: 0, max: 999 })


}

createPane()
canvasSketch(sketch, settings);
