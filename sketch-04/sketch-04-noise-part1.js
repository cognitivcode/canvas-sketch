const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');
const math = require('canvas-sketch-util/math');

const settings = {
  dimensions: [ 1080, 1080 ],
  animate: true
};

const sketch = () => {
  return ({ context, width, height, frame }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    // Define a number of columns and rows for the grid
    const cols = 10
    const rows = 10
    const numCells = cols * rows

    // Now we set up a grid of 80% of the total width and height
    const gridw = width * 0.8
    const gridh = height * 0.8
    // We calculate the width and height for each cell of the grid
    const cellw = gridw / cols
    const cellh = gridh / rows
    // We have to keep in mind the margin x and y position 
    const margx = (width - gridw) * 0.5
    const margy = (height - gridh) * 0.5

    for (let i = 0; i < numCells; i++) {
      // The Remainder operator helps us know wich col are we iterating
      const col = i % cols
      // And the floor operator of the division between the index and the number of cols would tell us the row we are iterating
      const row = Math.floor(i / cols)
      
      // Here we determine the x and y position of each cell of the iteration
      const x = col * cellw
      const y = row * cellh
      // And also here we declare the width and height of the line we want to draw
      const w = cellw * 0.8
      const h = cellh * 0.8

      const n = random.noise2D(x + frame, y, 0.001)
      const angle = n * Math.PI * 0.2
      // const scale = (n + 1) / 2 * 30
      // const scale = (n * 0.5 + 0.5) * 30
      const scale = math.mapRange(n, -1, 1, 1, 30)

      // We want to save the context and restore it on every iteration
      context.save()

      // We want to start to draw on the x and y position of each cell 
      context.translate(x, y)
      // But we have to add the margin x and y of the grid too
      context.translate(margx, margy)
      // And to move to the center of the cell and draw from there we do the following
      context.translate(cellw * 0.5, cellh * 0.5)

      context.rotate(angle)

      context.lineWidth = scale

      // Now that we are at the center of the cell we start our path
      context.beginPath()
      // Move half to the left
      context.moveTo(w * -0.5, 0)
      // And then draw the line to half to the right
      context.lineTo(w * 0.5, 0)
      // Resulting on a line of 80% of the cell width (due to the margin)
      context.stroke()

      context.restore()
    }
  };
};

canvasSketch(sketch, settings);
